#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

/usr/bin/xvfb-run -n 0 -s "-screen 0 1400x900x24" jupyter notebook $*
